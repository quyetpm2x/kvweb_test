import BoxDomainTemplate from './boxDomainTemplate';
import './boxDomain.scss';
import { useEffect, useState } from 'react';
import { domainDefault } from './boxDomainConst';

const BoxDomainComponent = () => {
  const domainFromSessionStorage = JSON.parse(sessionStorage.getItem('listDomain') || '[]');
  const [listDomain, setListDomain] = useState([{ ...domainDefault }]);

  useEffect(() => {
    if (domainFromSessionStorage.length) {
      setListDomain(domainFromSessionStorage);
    }
  }, []);

  const isValidDomain = (domainName: string) => {
    if (!domainName) return true;
    var re = /^(?!:\/\/)([a-zA-Z0-9-]+\.){0,5}[a-zA-Z0-9-][a-zA-Z0-9-]+\.[a-zA-Z]{2,64}?$/gi;
    return re.test(domainName);
  };

  const resetSessionStorage = () => {
    sessionStorage.setItem('step', String(1));
    sessionStorage.removeItem('domainCustomInfo');
    sessionStorage.removeItem('listDomain');
  };

  const checkTen = (number: number) => `${number < 10 ? '0' + number : number}`;

  const getDateTimeNow = () => {
    const currentdate = new Date();
    return `${checkTen(currentdate.getHours())} giờ ${checkTen(currentdate.getMinutes())} phút, ngày ${checkTen(
      currentdate.getDate()
    )}/${checkTen(currentdate.getMonth() + 1)}/${currentdate.getFullYear()}`;
  };

  const isEmptyObj = (obj: object) => {
    for (const prop in obj) {
      if (Object.hasOwn(obj, prop)) {
        return false;
      }
    }
    return true;
  };

  return (
    <>
      <BoxDomainTemplate
        listDomain={listDomain}
        setListDomain={setListDomain}
        isValidDomain={isValidDomain}
        resetSessionStorage={resetSessionStorage}
        getDateTimeNow={getDateTimeNow}
        isEmptyObj={isEmptyObj}
      />
    </>
  );
};

export default BoxDomainComponent;
