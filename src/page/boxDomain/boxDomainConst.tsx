export const domainDefault = {
  name: 'cuahangxxx.kiotvietweb.com',
  status: 'Đã kết nối',
  dateTime: '16 giờ 52 phút, ngày 27/03/2023',
  default: true,
};

export const domainProperty = {
  name: 'Tên miền',
  status: 'Trạng thái',
  dateTime: 'Ngày thêm',
};

export const domainStatus = {
  success: 'Đã kết nối',
  pending: 'Hệ thống đang kết nối',
};

export const defaultDomainInfo = {
  currentIP: '123.456.789',
  name: '',
  host: '@',
  type: 'A',
  status: '',
  dateTime: '',
};

export const domainPropertyTable = {
  host: 'Host',
  type: 'Loại',
  currentIP: 'Giá trị',
};

export const instructNote = [
  {
    name: 'matbao.vn',
    link: 'xem chi tiết',
  },
  {
    name: 'cloudflare',
    link: 'xem chi tiết',
  },
  {
    name: 'pavietnam',
    link: 'xem chi tiết',
  },
  {
    name: 'Nhà cung cấp khác',
    link: 'xem thêm',
  },
];
