import BorderColorOutlinedIcon from '@mui/icons-material/BorderColorOutlined';
import CheckCircleIcon from '@mui/icons-material/CheckCircle';
import InfoIcon from '@mui/icons-material/Info';
import WarningAmberRoundedIcon from '@mui/icons-material/WarningAmberRounded';
import Button from '@mui/material/Button';
import TextField from '@mui/material/TextField';
import clsx from 'clsx';
import { useEffect, useState } from 'react';
import { Link } from 'react-router-dom';
import CommonDomainItem from '../../component/commonDomainItem';
import CustomizedTables from '../../component/dataTable';
import { defaultDomainInfo, domainProperty, domainStatus, instructNote } from './boxDomainConst';
interface Props {
  listDomain: object[];
  setListDomain: any;
  isValidDomain: (name: string) => boolean;
  resetSessionStorage: () => void;
  getDateTimeNow: () => void;
  isEmptyObj: (str: object) => boolean;
}

interface domainInfo {
  currentIP: string;
  status: string;
  name: string;
  host: string;
  type: string;
}

const BoxDomainTemplate = ({
  listDomain,
  setListDomain,
  isValidDomain,
  resetSessionStorage,
  getDateTimeNow,
  isEmptyObj,
}: Props) => {
  const listLength = listDomain.length;
  const domainCustomInfoFromSessionStorage = sessionStorage.getItem('domainCustomInfo');
  const [validateAddDomain, setValidateAddDomain] = useState(true);
  const [valueInput, setValueInput] = useState('');
  const [step, setStep] = useState(Number(sessionStorage.getItem('step')) || 1);
  const [domainCustomInfo, setDomainCustomInfo] = useState<domainInfo>({ ...defaultDomainInfo });

  useEffect(() => {
    if (!isEmptyObj(JSON.parse(domainCustomInfoFromSessionStorage || '{}'))) {
      setDomainCustomInfo(JSON.parse(domainCustomInfoFromSessionStorage || '{}'));
    }
  }, []);

  const handleChangeInput = (e: any) => {
    setValueInput(e.target.value);
    setValidateAddDomain(isValidDomain(e.target.value));
  };
  const handHelperText = () => {
    if (!validateAddDomain) return 'Tên miền không đúng định dạng';
    if (valueInput && validateAddDomain) return 'Tên miền hợp lệ';
    return '';
  };

  const handleClickLeft = () => {
    if (step === 4) {
      setListDomain((preState: any) => [{ ...preState[0] }]);
      sessionStorage.setItem('listDomain', JSON.stringify([{ ...listDomain[0] }]));
      setValueInput('');
    }
    setStep(1);
    resetSessionStorage();
  };

  const handleNextStep = () => {
    setStep((step) => step + 1);
    sessionStorage.setItem('step', String(step + 1));
    if (step === 1) {
      setDomainCustomInfo({
        ...domainCustomInfo,
        name: valueInput,
        status: '',
      });
      sessionStorage.setItem(
        'domainCustomInfo',
        JSON.stringify({
          ...domainCustomInfo,
          name: valueInput,
          status: '',
        })
      );
    }
    if (step === 2) {
      setDomainCustomInfo({
        ...domainCustomInfo,
        status: domainStatus.pending,
      });
      sessionStorage.setItem(
        'domainCustomInfo',
        JSON.stringify({
          ...domainCustomInfo,
          status: domainStatus.pending,
        })
      );
    }
    if (step === 3) {
      sessionStorage.setItem(
        'domainCustomInfo',
        JSON.stringify({
          ...domainCustomInfo,
          dateTime: getDateTimeNow(),
          status: domainStatus.success,
        })
      );
      sessionStorage.setItem(
        'listDomain',
        JSON.stringify([
          ...listDomain,
          {
            ...domainCustomInfo,
            dateTime: getDateTimeNow(),
            status: domainStatus.success,
          },
        ])
      );
      setListDomain((preState: any) => [
        ...preState,
        {
          ...domainCustomInfo,
          dateTime: getDateTimeNow(),
          status: domainStatus.success,
        },
      ]);
    }
  };

  const dataRows = [domainCustomInfo];

  return (
    <div className='domain__template--wrapper'>
      <div className='domain__template--header'>
        <div className='title'>Tên miền</div>
        <div className='desc'>Tăng khả năng hiển thị trang website của bạn</div>
        <div className='instructions'>
          Bạn có thể <Link to='/'>xem hướng dẫn tại đây</Link>
        </div>
      </div>
      <div className='domain__template--body'>
        {listDomain.map((domain: any) => {
          if (domain.default) return <CommonDomainItem domainInfo={domain} />;
          return (
            <div
              key={domain.name}
              className={clsx('', {
                'mb-48': step === 4,
              })}
            >
              <CommonDomainItem domainInfo={domain} onClickLeft={handleClickLeft} iconDotLeft={true} />
            </div>
          );
        })}
        {listLength < 2 && (
          <div
            className={clsx('add__domain--form domain__item--wrapper', {
              outline: step === 2 || step === 3,
            })}
          >
            {step === 1 && (
              <>
                <div className='common__item--header'>
                  <div className='title'>Tên miền tùy chỉnh</div>
                </div>
                <div className='body--step1 body'>
                  <div className='input--wrapper'>
                    <TextField
                      focused={!!valueInput}
                      value={valueInput}
                      onChange={handleChangeInput}
                      error={!validateAddDomain}
                      id='outlined-password-input'
                      label='Nhập tên miền của bạn'
                      type='text'
                      className='add__domain--input'
                      helperText={handHelperText()}
                    />
                    {!validateAddDomain && (
                      <InfoIcon fontSize='small' style={{ color: 'red' }} className='info-icon'></InfoIcon>
                    )}
                    {validateAddDomain && valueInput && (
                      <CheckCircleIcon fontSize='small' style={{ color: 'green' }} className='info-icon' />
                    )}
                  </div>
                </div>
              </>
            )}
            {(step === 2 || step === 3) && (
              <div className='body--step2 body'>
                <CommonDomainItem
                  domainInfo={domainCustomInfo}
                  property={domainProperty}
                  title='Tên miền tùy chỉnh'
                  btnLeftName='Thay đổi tên miền'
                  btnLeftIcon={<BorderColorOutlinedIcon />}
                  onClickLeft={handleClickLeft}
                  timmer={step === 3}
                  onNextStep={handleNextStep}
                />
                <div className='property-add-new-domain'>
                  {step === 2 && (
                    <div className='item__property item__currentIP'>
                      <div className='left'>IP hiện tại</div>
                      <div className='right current-ip'>
                        <span>{domainCustomInfo.currentIP}</span>
                        <CheckCircleIcon fontSize='small' style={{ color: 'green' }} className='info-icon' />
                      </div>
                    </div>
                  )}
                  <div className='ip__kiot-viet-web item__property'>
                    <div className='title'>IP KiotVietWeb:</div>
                    <div className='table'>
                      <CustomizedTables dataRows={dataRows} />
                    </div>
                  </div>
                </div>
                <div className='custom__domain--note'>
                  {step === 2 && (
                    <>
                      <div className='icon'>
                        <WarningAmberRoundedIcon />
                      </div>
                      <div>
                        <div className='extention'>
                          <div className='content'>
                            <span className='title'>Lưu ý: </span>
                            <span className='desc'>
                              Nhập đúng địa chỉ IP KVWEB sang nhà cung cấp. Thời gian kết nối IP sẽ tùy thuộc vào nhà
                              cung cấp (khoảng 2-5 tiếng)
                            </span>
                          </div>
                        </div>
                        <div className='intruct--wrapper'>
                          <div className='title'>Hướng dẫn chi tiết từ nhà cung cấp:</div>
                          <ul className='intruct--list'>
                            {instructNote.map((intructItem: any) => {
                              return (
                                <li>
                                  <div className='name-link'>
                                    <div className='name'>{intructItem.name}</div>
                                    <Link to='/' className='link'>
                                      {intructItem.link}
                                    </Link>
                                  </div>
                                </li>
                              );
                            })}
                          </ul>
                        </div>
                      </div>
                    </>
                  )}
                  {step === 3 && (
                    <div className='extention'>
                      Hệ thống đang thiết lập tên miền. Bạn có thể truy cập tên miền sau
                      <span className='title'> 10 phút</span>, nếu không được vui lòng liên hệ hotline{' '}
                      <span className='title'> 1900 6522 </span> để được hỗ trợ.
                    </div>
                  )}
                </div>
              </div>
            )}
            {(step === 1 || step === 2) && (
              <div className='domain__template--footer'>
                <Button
                  variant='contained'
                  size='small'
                  disabled={!validateAddDomain || (!valueInput && step === 1)}
                  onClick={handleNextStep}
                >
                  {step === 1 ? 'Tiếp tục' : 'Kết nối'}
                </Button>
              </div>
            )}
          </div>
        )}
      </div>
    </div>
  );
};

export default BoxDomainTemplate;
