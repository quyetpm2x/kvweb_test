import BoxDomainComponent from './page/boxDomain/boxDomainComponent';
import { BrowserRouter as Router } from 'react-router-dom';
function App() {
  return (
    <Router>
      <div className='App'>
        <BoxDomainComponent />
      </div>
    </Router>
  );
}

export default App;
