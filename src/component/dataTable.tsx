import { styled } from '@mui/material/styles';
import Table from '@mui/material/Table';
import TableBody from '@mui/material/TableBody';
import TableCell, { tableCellClasses } from '@mui/material/TableCell';
import TableContainer from '@mui/material/TableContainer';
import TableHead from '@mui/material/TableHead';
import TableRow from '@mui/material/TableRow';
import Paper from '@mui/material/Paper';
import { domainPropertyTable } from '../page/boxDomain/boxDomainConst';
import ContentCopyIcon from '@mui/icons-material/ContentCopy';

const StyledTableCell = styled(TableCell)(() => ({
  [`&.${tableCellClasses.head}`]: {
    backgroundColor: '#d7d7d7',
    color: '#828282',
    fontWeight: '400',
    fontSize: 14,
    padding: '8px',
  },
  [`&.${tableCellClasses.body}`]: {
    fontSize: 14,
    padding: '8px',
  },
}));

const StyledTableRow = styled(TableRow)(({ theme }) => ({
  '&:nth-of-type(odd)': {
    backgroundColor: theme.palette.action.hover,
  },
  // hide last border
  '&:last-child td, &:last-child th': {
    border: 0,
  },
}));

interface Props {
  dataRows: any;
}

const copyContent = async (text: string) => {
  try {
    await navigator.clipboard.writeText(text);
    console.log('Content copied to clipboard');
  } catch (err) {
    console.error('Failed to copy: ', err);
  }
};

const CustomizedTables = ({ dataRows = [] }: Props) => {
  return (
    <TableContainer component={Paper}>
      <Table aria-label='customized table'>
        <TableHead>
          <TableRow>
            {[...Object.keys(domainPropertyTable)].map((item) => (
              <StyledTableCell align='left' sx={{ width: item === 'currentIP' ? '50%' : '25%' }} key={item}>
                {domainPropertyTable[item as keyof typeof domainPropertyTable]}
              </StyledTableCell>
            ))}
          </TableRow>
        </TableHead>
        <TableBody>
          {dataRows.map((row: any) => (
            <StyledTableRow key={row.name}>
              <StyledTableCell align='left'>{row.host}</StyledTableCell>
              <StyledTableCell align='left'>{row.type}</StyledTableCell>
              <StyledTableCell align='left' className='currentIP-value'>
                <div className='currentIP'>{row.currentIP}</div>
                <div className='copy__value' onClick={() => copyContent(row.currentIP)}>
                  <ContentCopyIcon />
                  <div className='copy-text'>Sao chép</div>
                </div>
              </StyledTableCell>
            </StyledTableRow>
          ))}
        </TableBody>
      </Table>
    </TableContainer>
  );
};

export default CustomizedTables;
