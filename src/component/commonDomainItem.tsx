import clsx from 'clsx';
import { domainProperty, domainStatus } from '../page/boxDomain/boxDomainConst';
import LeftItemComponent from './dropdownLeftItem';
import TimerComponent from './timerComponent';
interface Props {
  domainInfo?: any;
  property?: object;
  title?: string;
  btnLeftName?: any;
  btnLeftIcon?: any;
  onClickLeft?: () => void;
  onNextStep?: () => void;
  timmer?: boolean;
  iconDotLeft?: boolean;
}

const CommonDomainItem = ({
  domainInfo = {},
  property = { ...domainProperty },
  title = '',
  btnLeftName = null,
  btnLeftIcon = null,
  onClickLeft = () => {},
  onNextStep = () => {},
  timmer = false,
  iconDotLeft = false,
}: Props) => {
  return (
    <div className='domain__item--wrapper'>
      <div className='common__item--header'>
        <div className='title'>{title || (domainInfo.default ? 'Tên miền mặc định' : 'Tên miền hiện tại')}</div>
        {(btnLeftName || btnLeftIcon) && (
          <div className='btn-left' onClick={onClickLeft}>
            {btnLeftIcon && <div className='icon'>{btnLeftIcon}</div>}
            <div className='text'>{btnLeftName}</div>
          </div>
        )}
        {iconDotLeft && <LeftItemComponent onClickLeft={onClickLeft} />}
      </div>
      <div className='body'>
        {timmer && <TimerComponent onNextStep={onNextStep} />}
        {[...Object.keys(property)].map((item: string) => {
          if (!domainInfo[item]) return null;
          return (
            <div className={`item__property item__${item}`} key={item}>
              <div className='left'>{property[item as keyof typeof property]}</div>
              <div
                className={clsx('right', {
                  success: domainInfo[item] === domainStatus.success && item === 'status',
                  pending: domainInfo[item] === domainStatus.pending && item === 'status',
                })}
              >
                {domainInfo[item]}
              </div>
            </div>
          );
        })}
      </div>
    </div>
  );
};

export default CommonDomainItem;
