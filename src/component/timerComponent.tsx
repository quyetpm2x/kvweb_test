import React, { useEffect, useState } from 'react';
import HourglassEmptyOutlinedIcon from '@mui/icons-material/HourglassEmptyOutlined';
interface Props {
  onNextStep: () => void;
}
const TimerComponent = ({ onNextStep }: Props) => {
  const [minute, setMinute] = useState(10);
  const [seconds, setSeconds] = useState(0);
  const countTimer = () => {
    if (seconds >= 0) {
      setTimeout(() => {
        if (minute === 0 && seconds === 0) onNextStep();
        else setSeconds((prevState) => prevState - 1);
      }, 1000);
    }

    if (seconds < 0 && minute > 0) {
      setMinute((prevState) => prevState - 1);
      setSeconds(59);
    }
  };
  useEffect(() => {
    countTimer();
  }, [seconds]);

  return (
    <div className='timer--wrapper'>
      <div className='icon'>
        <HourglassEmptyOutlinedIcon />
      </div>
      <div className='time'>
        {minute < 10 ? `0${minute}` : minute}:{seconds < 10 ? `0${seconds}` : seconds}s
      </div>
    </div>
  );
};

export default TimerComponent;
